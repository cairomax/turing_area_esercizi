int *A;

int ordina(int n)
{
  A = new int[n];

  for(int i = 0; i < n; i++) {
    A[i] = i;
  }

  bool scambio;
  do {
    scambio = false;
    for (int i=0; i < n-1; i++) {
	  int compare = pesa(A[i],A[i+1]);
      if ( compare == 1){
        //scambio i e i+1
        int temp = A[i+1];
        A[i+1] = A[i];
        A[i] = temp;
        scambio = true;
      }
    }
  } while(scambio);
  return 1;
}

int elemento(int i)
{
  return A[i];
}
