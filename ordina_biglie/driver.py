import sys

from interfaces.ordina_biglie import ordina_biglie
from turingarena.runtime.sandbox import sandbox
import random
from math import log, ceil, floor

random.seed(0) # make it deterministic

class ordina_biglie_utils:
    def __init__(self, driver):
        self.driver = driver
        self.biglie = list(range(driver.N))
        random.shuffle(self.biglie)
        self.biglieOrd = [None] * driver.N
        self.pesate = 0

    #pesa due biglie e ritorna
    # 1 se la bigliaA è più pesante
    # 0 se sono uguali
    # -1 se è più pesante la bigliaB
    #def pesa(self, p1, p2):
    def pesa(self, p1, p2):
        assert p1 != p2
        self.pesate += 1
        if self.biglie[p1] > self.biglie[p2]:
            return 1
        elif self.biglie[p1] < self.biglie[p2]:
            return -1
        assert False


def checkOrderArray(utils, N):
    return all(utils.biglie[utils.biglieOrd[i]] == i for i in range(N))

def evaluate_solution(N):
    with sandbox.create_process("solution") as s, ordina_biglie(s) as driver:
        driver.N = N
        ob = ordina_biglie_utils(driver)
        #problema con il parser
        S = driver.ordina(N, callback_pesa=ob.pesa)
        for i in range(N):
            ob.biglieOrd[i] = driver.elemento(i)
        return (checkOrderArray(ob, driver.N), ob.pesate)

task0 = True
task1 = True
task2 = True
task3 = True
task4 = True
task5 = True

for N in [3, 10, 100]:
    (answer_is_correct, number_of_weights) = evaluate_solution(N)
    task0 = task0 & answer_is_correct

for N in [3, 10,100]:
    (answer_is_correct, number_of_weights) = evaluate_solution(N)
    task1 = task1 & answer_is_correct

(answer_is_correct,number_of_weights) = evaluate_solution(7)
task2 = answer_is_correct & (number_of_weights <= 21)

(answer_is_correct,number_of_weights) = evaluate_solution(7)
task3 = answer_is_correct & (number_of_weights <= 5*7*ceil(log(7)))

(answer_is_correct,number_of_weights) = evaluate_solution(7)
task4 = answer_is_correct & (number_of_weights <= 7*ceil(log(7)))

(answer_is_correct,number_of_weights) = evaluate_solution(8)
task5 = answer_is_correct & (number_of_weights <=7*(ceil(log(7) -1)))

print("Task0:", task0, file=sys.stderr)
print("Task1:", task1, file=sys.stderr)
print("Task2:", task2, file=sys.stderr)
print("Task3:", task3, file=sys.stderr)
print("Task4:", task4, file=sys.stderr)
print("Task5:", task5, file=sys.stderr)
