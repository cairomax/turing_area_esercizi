from __future__ import print_function
from abc import abstractmethod
from turingarena.runtime.driver import *
from turingarena.runtime.data import *



class Engine(BaseDriverEngine):

    @staticmethod
    def global_data(var, globals):

        # var int N;
        N_, = var(scalar(int)),
        globals.N = N_

        # function ordina(int n) -> int;
        pass

        # function elemento(int i) -> int;
        pass

        # callback pesa(int p1, int p2) -> int...
        pass

        # main {...
        pass

    @staticmethod
    def porcelain(var, flush):

        def accept_callbacks():
            while True:
                callback_, = var(scalar(str)),
                pass
                if get_value(callback_) == 'return': break
                elif get_value(callback_) == 'pesa': yield from callback_pesa()
                else: raise ValueError('unexpected callback %s' % get_value(callback_))

        # var int N;
        N_, = var(scalar(int)),

        # function ordina(int n) -> int;
        pass

        # function elemento(int i) -> int;
        pass

        # callback pesa(int p1, int p2) -> int...
        def callback_pesa():
            # <callback arguments>
            p1_, p2_, = var(scalar(int)), var(scalar(int)),
            # <callback initialization>
            command = yield 'callback_started', 'pesa', (p1_, p2_, )
            # expected calls: None
            # output p1, p2;
            pass
            # flush;
            flush()
            # var int ans;
            ans_, = var(scalar(int)),
            # input ans;
            pass
            # return ans;
            expect_return(command, ans_)
            yield 'return_accepted',
            # <callback finalization>
            yield 'callback_stopped',

        # main {...
        def main():
            # <main initialization>
            command = yield 'main_started',
            # expected calls: None
            # input N;
            pass
            # var int junk;
            junk_, = var(scalar(int)),
            # call ordina(N) -> junk;
            expect_call(command, 'ordina', [N_, ])
            yield 'call_accepted',
            yield from accept_callbacks()
            command = yield 'call_returned', 'ordina', get_value(junk_)
            # output junk;
            pass
            # for (i:N) {...
            for index_i in range(get_value(N_)):
                i_ = constant(scalar(int), index_i)
                # expected calls: {'elemento'}
                # var int ans;
                ans_, = var(scalar(int)),
                # call elemento(i) -> ans;
                expect_call(command, 'elemento', [i_, ])
                yield 'call_accepted',
                yield from accept_callbacks()
                command = yield 'call_returned', 'elemento', get_value(ans_)
                # output ans;
                pass
            # <main finalization>
            yield 'main_stopped',

        return main()

    @staticmethod
    def plumbing(var, upward_pipe, downward_pipe):

        def accept_callbacks():
            while True:
                callback_, = var(scalar(str)),
                read([(str, callback_)], file=upward_pipe)
                if get_value(callback_) == 'return': break
                elif get_value(callback_) == 'pesa': yield from callback_pesa()
                else: raise ValueError('unexpected callback %s' % get_value(callback_))

        # var int N;
        N_, = var(scalar(int)),

        # function ordina(int n) -> int;
        pass

        # function elemento(int i) -> int;
        pass

        # callback pesa(int p1, int p2) -> int...
        def callback_pesa():
            # <callback arguments>
            p1_, p2_, = var(scalar(int)), var(scalar(int)),
            # <callback initialization>
            pass
            # expected calls: None
            # output p1, p2;
            read([(int, p1_), (int, p2_)], file=upward_pipe)
            # flush;
            yield
            # var int ans;
            ans_, = var(scalar(int)),
            # input ans;
            print(get_value(ans_), file=downward_pipe, flush=True)
            # return ans;
            pass
            # <callback finalization>
            yield from []

        # main {...
        def main():
            # <main initialization>
            pass
            # expected calls: None
            # input N;
            print(get_value(N_), file=downward_pipe, flush=True)
            # var int junk;
            junk_, = var(scalar(int)),
            # call ordina(N) -> junk;
            yield from accept_callbacks()
            # output junk;
            read([(int, junk_)], file=upward_pipe)
            # for (i:N) {...
            for index_i in range(get_value(N_)):
                i_ = constant(scalar(int), index_i)
                # expected calls: {'elemento'}
                # var int ans;
                ans_, = var(scalar(int)),
                # call elemento(i) -> ans;
                yield from accept_callbacks()
                # output ans;
                read([(int, ans_)], file=upward_pipe)
            # <main finalization>
            yield

        return main()


class Driver(BaseDriver):
    _engine_class = Engine

    # var int N;
    @property
    def N(self):
        return get_value(self._engine.globals.N)

    @N.setter
    def N(self, value):
        set_value(self._engine.globals.N, value)

    # function ordina(int n) -> int;
    def ordina(self, arg_n, **kwargs):
        return self._engine.call("ordina", arg_n, has_return=True, **kwargs)

    # function elemento(int i) -> int;
    def elemento(self, arg_i, **kwargs):
        return self._engine.call("elemento", arg_i, has_return=True, **kwargs)

    # callback pesa(int p1, int p2) -> int...
    pass

    # main {...
    pass


ordina_biglie = Driver
